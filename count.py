
import sys

counts = {}

for line in sys.stdin:
  for rawword in line.split():
    word = rawword.strip()
    cnt = counts.get(word, 0)
    counts[word] = cnt + 1

sorted_counts = sorted(counts.items(), key=lambda i: i[1], reverse=True)

for word, count in sorted_counts:
  print("{0} {1}".format(count, word))


