import org.scalatest.FunSuite

class SolutionTest extends FunSuite {

  class Node(val name:String, val children:List[Node]) extends GNode {
    override def getName: String = name
    override def getChildren: List[GNode] = children
    override def toString = name
  }

  object Node {
    def apply(name:String, children:Node*) = new Node(name, children.toList)
    def apply(name:String) = new Node(name, Nil)
  }

  test("Walk over single node") {
    val node = Node("root")
    assert(Solution.walkGraph(node) === List(node))
  }

  test("Walk over simple graph") {
    val leaf1 = Node("l1")
    val leaf2 = Node("l2")
    val node1 = Node("n1", leaf1, leaf2)
    val root = Node("root", node1)
    assert(Solution.walkGraph(root).toSet === Set(leaf1, leaf2, node1, root))
  }


  test("Nodes should be de-duplicated") {
    val leaf1 = Node("l1")
    val leaf2 = Node("l2")
    val node1 = Node("n1", leaf1, leaf2)
    val root = Node("root", node1, leaf1, leaf2)
    assert(Solution.walkGraph(root).toSet === Set(leaf1, leaf2, node1, root))
  }

  test("Paths in single-node graph") {
    val root = Node("root")
    assert(Solution.paths(root).toSet === Set(List(root)))
  }

  test("Paths in multiple-node graph") {
    val leaf1 = Node("l1")
    val leaf2 = Node("l2")
    val node1 = Node("n1", leaf1, leaf2)
    val root = Node("root", node1, leaf1)
    assert(Solution.paths(root).toSet === Set(
      List(root, node1, leaf1),
      List(root, node1, leaf2),
      List(root, leaf1)
    ))
  }

}
