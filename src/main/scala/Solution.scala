
import scala.collection._

trait GNode {
  def getName: String
  def getChildren: List[GNode]
}

object Solution {

  def walkGraph(node: GNode): List[GNode] = {

    val visited = mutable.Set[GNode]()

    def recurse(node: GNode) {
      if (!visited.contains(node)) {
        visited += node
        for (child <- node.getChildren) recurse(child)
      }
    }

    recurse(node)
    visited.toList
  }

  def paths(node: GNode): List[List[GNode]] = {

    def recurse(node: GNode, prefix:List[GNode]): List[List[GNode]] = {
      val path = node :: prefix
      if (node.getChildren.isEmpty)
        List(path.reverse)
      else {
        for {
          child <- node.getChildren
          childPath <- recurse(child, path)
        } yield (childPath)
      }
    }

    recurse(node, Nil)
  }

}